module.exports = {
  root: true,
  plugins: ['import'],
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],
  overrides: [
    {
      files: ['**/*.js'],
      rules: {
        'no-unused-vars': ['off'],
        'no-undef': ['off'],
      },
      parserOptions: {
        parser: 'babel-eslint',
      },
    },
  ],
  rules: {
    'import/prefer-default-export': 'off',
    'no-underscore-dangle': 'off',
  },
};
