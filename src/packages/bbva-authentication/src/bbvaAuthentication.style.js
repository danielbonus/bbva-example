import { css } from 'lit-element';

export const styles = css`
  :host {
    display: block;
    font-family: "Andale Mono";
  }

  section {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  form {
    display: flex;
    flex-direction: column;
    width: 50%;
  }
`;
