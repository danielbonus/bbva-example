import { LitElement, html } from 'lit-element';
import { styles } from './bbvaAuthentication.style';

export class BbvaAuthentication extends LitElement {
  static get styles() {
    return styles;
  }

  handleSubmit(event) {
    event.preventDefault();
    const inputEmail = event.currentTarget.querySelector('input[name="email"]').value;
    const inputPassword = event.currentTarget.querySelector('input[name="password"]').value;

    if (!!inputEmail.length && !!inputPassword.length) {
      this.dispatchEvent(new CustomEvent('on-bbva-submit', {
        bubbles: true,
        composed: true,
        detail: {
          email: inputEmail,
          password: inputPassword,
        },
      }));
    }
  }

  render() {
    return html`
      <section>
        <img src="" alt="">
        <form @submit="${this.handleSubmit}">
          <input name="email" type="email">
          <input name="password" type="password">
          <button type="submit">Login</button>
        </form>
      </section>

    `;
  }
}
