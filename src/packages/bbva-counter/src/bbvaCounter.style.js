import { css } from 'lit-element';

export const style = css`
  :host {
    display: block;
    font-family: "Andale Mono";
  }

  .container-counter {
    display: flex;
    justify-content: space-around;
    align-items: center;
    list-style-type: none;
    margin: 0;
    padding: 0;
  }

  .container-counter li {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column-reverse;
  }

  .number-counter {
    color: gray;
    font-size: 32px;
    margin-bottom: 16px;
  }
`;
