import { LitElement, html } from 'lit-element';
import { style } from './bbvaCounter.style';
import { timeDiffCalc } from '../utils/helperCalculeDiff';



export class BbvaCounter extends LitElement {
  static get properties() {
    return {
      startTime: { type: String },
      countTime: { type: Object },
      nowTime: { type: Object },
    };
  }

  static get styles() {
    return style;
  }

  firstUpdated(changedProperties) {
    super.firstUpdated(changedProperties);
    this.countTime = { ...this.countTime, ...this.calculateDiff(this.startTime) };
  }

  // eslint-disable-next-line class-methods-use-this
  calculateDiff(startTime) {
    this.nowTime = new Date();
    return timeDiffCalc(startTime, this.nowTime);
  }

  constructor() {
    super();
    this.startTime = '';
    this.countTime = {};
  }

  render() {
    return html`
        <section>
          <ul class="container-counter">
            ${Object.keys(this.countTime).map((time) => html`
                <li>
                  <span class="title-counter">${time}</span>
                  <p class="number-counter">${this.countTime[time]}</p>
                </li>
            `)}
          </ul>

        </section>
    `;
  }
}
