const secondsInMilliSeconds = 1000;
const secondsInMinute = 60;
const secondsInHour = 60 * 60;
const secondsInDay = 60 * 60 * 24;

const parseMetaDateLogin = (startTime) => {
  const parseStringDate = startTime.split(' ');
  const parseTimeDate = parseStringDate[4].split(':');
  const startDate = new Date(`${parseStringDate[0]}, ${parseStringDate[1]} ${parseStringDate[2]} ${parseStringDate[3]}`);
  startDate.setHours(parseTimeDate[0]);
  startDate.setMinutes(parseTimeDate[1]);
  startDate.setSeconds(parseTimeDate[2]);
  return startDate;
};

export const timeDiffCalc = (startTime, dateNow) => {
  const parseStartTime = parseMetaDateLogin(startTime);
  let diffInMilliSeconds = Math.abs(parseStartTime - dateNow) / secondsInMilliSeconds;

  const days = Math.floor(diffInMilliSeconds / secondsInDay);
  diffInMilliSeconds -= days * secondsInDay;

  const hours = Math.floor(diffInMilliSeconds / secondsInHour) % 24;
  diffInMilliSeconds -= hours * secondsInHour;

  const minutes = Math.floor(diffInMilliSeconds / secondsInMinute) % secondsInMinute;
  diffInMilliSeconds -= minutes * secondsInMinute;

  const seconds = Math.round(diffInMilliSeconds);
  diffInMilliSeconds -= seconds;

  return {
    days,
    hours,
    minutes,
    seconds,
  };
};
