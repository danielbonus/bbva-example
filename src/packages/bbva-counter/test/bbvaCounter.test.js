import { fixture, html, expect } from '@open-wc/testing';
import '../bbva-counter';
import { timeDiffCalc } from '../utils/helperCalculeDiff';

describe('bbva-counter', () => {
  let el;
  beforeEach(async () => {
    el = await fixture(html` <bbva-counter .startTime="${'Wed, 16 Jun 2021 11:21:18 GMT'}"></bbva-counter> `);
  });

  it('should calculated diff start date from now date', async () => {
    await el.updateComplete;
    const nowDate = new Date(el.nowTime);
    const diff = timeDiffCalc('Wed, 16 Jun 2021 11:21:18 GMT', nowDate);
    expect(diff).to.include(el.countTime);
  });

  it('should render count', async () => {
    await el.updateComplete;
    const numberItemsRender = el.shadowRoot.querySelector('.container-counter').childElementCount;
    expect(numberItemsRender).to.be.equal(4);
  });
});
