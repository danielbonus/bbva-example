import { Router } from '@vaadin/router';
import { dedupeMixin } from '@open-wc/dedupe-mixin';
import { isLoggedIn } from '../login/statusLogin';



export const RouterMixin = dedupeMixin( superClass =>
  class RouterMixin extends  superClass {

    constructor(...args) {
      super(args);
      if(!this.routes.length) {
        throw new Error('need path routes');
      }
    }

    firstUpdated(_changedProperties) {
      super.firstUpdated(_changedProperties);
      this._router = new Router(this.shadowRoot.querySelector('#content-router'));
      if(this.routes.length) {
        const routesWithAction = this._handleActionPath(this.routes);
        this._router.setRoutes([...this._router.getRoutes(), ...routesWithAction]);
      }
    }

    _handleActionPath(routesProtected) {
      return [...routesProtected.reduce((total, current) => {
        this._setProtectedPath(current);
        return [...total,  current];
      }, [])];
    }


    _setProtectedPath(current) {
      if(current.protected) {
        current.action = async (ctx, commands) => {
          if(!await isLoggedIn()) {
            return commands.redirect('/');
          }
          return await import(current.componentPath)
        }
      } else {
        current.action = async () => {
          return await import(current.componentPath)
        }
      }
    }
  }
);

export const RouterProvider = dedupeMixin( superClass =>
  class RouterProvider extends superClass {
      navigator(path) {
        Router.go(path)
      }
  }
);


