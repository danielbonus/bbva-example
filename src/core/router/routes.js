export const pathRoutes = [
  {
    path: '/',
    component: 'bbva-login',
    protected: false,
    componentPath: '../../pages/bbva-login/bbva-login.js',
  },
  {
    path: '/home',
    component: 'bbva-home',
    protected: true,
    componentPath: '../../pages/bbva-home/bbva-home.js',
  },
];
