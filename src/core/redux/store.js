import { createStore } from 'redux';

const INITIALSTATE = {
  profileData: {},
};

const reducer = (state = INITIALSTATE, actions) => {
  switch (actions.type) {
    case 'PROFILE_DATA':
      return {
        ...state,
        profileData: actions.payload,
      };
    case 'CLEAN_DATA':
      return {
        ...INITIALSTATE,
      };
    default:
      return state;
  }
};

export const store = createStore(
  reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
