window.process = { env: { NODE_ENV: 'production' } };

export const connectElement = (store) => (baseElement) => class extends baseElement {
  connectedCallback() {
    if (super.connectedCallback) {
      super.connectedCallback();
    }
    this._storeUnsubscribe = store.subscribe(() => this.stateChanged(store.getState()));
    this.stateChanged(store.getState());
  }

  disconnectedCallback() {
    this._storeUnsubscribe();
    if (super.disconnectedCallback) {
      super.disconnectedCallback();
    }
  }

  // eslint-disable-next-line class-methods-use-this
  get storeState() {
    return store.getState();
  }

  // eslint-disable-next-line class-methods-use-this
  get storeDispatch() {
    return store.dispatch;
  }

  // eslint-disable-next-line class-methods-use-this
  stateChanged(_state) {
  }
};
