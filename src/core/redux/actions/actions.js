export const actionsBbvaExample = {
  profileData: (search) => ({
    type: 'PROFILE_DATA',
    payload: {
      ...search,
    },
  }),
  cleanData: () => ({
    type: 'CLEAN_DATA',
  }),
};
