import firebase from 'firebase';

const INITIAL_CONFIG_FIREBASE = {
  apiKey: 'AIzaSyC2dP490NeStllGpaUSkWF8Ry0LXgIrDIA',
  authDomain: 'bbvaexample.firebaseapp.com',
  projectId: 'bbvaexample',
  storageBucket: 'bbvaexample.appspot.com',
  messagingSenderId: '120940469825',
  appId: '1:120940469825:web:17d7b438d952c9a4cdf0c0',
  measurementId: 'G-FMNF9Q79QX',
};

const CODE_ERROR_LOGIN = {
  alreadyLogin: 'auth/email-already-in-use',
};

class Login {
  constructor() {
    this.firebaseInit = firebase.initializeApp(INITIAL_CONFIG_FIREBASE);
    this.auth = this.firebaseInit.auth();
    this.auth.onAuthStateChanged((user) => {
      this.isLogin = user !== null;
    });
  }

  isLoggedInPromise() {
    return new Promise((resolve, reject) => {
      this.auth.onAuthStateChanged((user) => resolve(user));
    });
  }

  async loginWithEmailPassword(email, password) {
    try {
      const resultSingIn = await this.auth.signInWithEmailAndPassword(email, password);
      const dataUser = {
        lastSignInTime: resultSingIn.user.metadata.lastSignInTime,
        email: resultSingIn.user.email,
        refreshToken: resultSingIn.user.refreshToken,
      };

      this._registerDataLogin(dataUser);
      return dataUser;
    } catch (err) {
      return err;
    }
  }

  async registerUser(data) {
    let _isRegister;
    try {
      _isRegister = await this.auth.createUserWithEmailAndPassword(data.email, data.password);
    } catch (err) {
      _isRegister = CODE_ERROR_LOGIN.alreadyLogin === 'auth/email-already-in-use' ? await this.loginWithEmailPassword(data.email, data.password) : false;
    }
    return _isRegister;
  }

  // eslint-disable-next-line class-methods-use-this
  _registerDataLogin({ lastSignInTime, email, refreshToken }) {
    sessionStorage.setItem('accessToken', refreshToken);
    sessionStorage.setItem('userInfo', JSON.stringify({ lastSignInTime, email }));
  }

  async singOut() {
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('userInfo');
    return !await this.auth.signOut();
  }
}
export default new Login();
