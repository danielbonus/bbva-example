import Login from './login';

export const isLoggedIn = () => Login.isLoggedInPromise();
