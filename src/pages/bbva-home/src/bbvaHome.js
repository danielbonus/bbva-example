import { LitElement, html } from 'lit-element';
import { connectElement } from '../../../core/redux/reduxMixin';
import { store } from '../../../core/redux/store';
import '../../../packages/bbva-counter/bbva-counter';
import { style } from './bbvaHome.style';

export class BbvaHome extends connectElement(store)(LitElement) {
  static get properties() {
    return {
      profileData: { type: Object },
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.profileData = {};
  }

  handleLogout() {
    this.dispatchEvent(new CustomEvent('on-bbva-logout', {
      bubbles: true,
      composed: true,
    }));
  }

  stateChanged(state) {
    this.profileData = { ...state.profileData };
  }

  render() {
    return html`
      <section class="container-home">
        <h1>Welcome!</h1>
        <p>The last time you accessed was</p>
        <bbva-counter .startTime="${this.profileData.lastSignInTime}"></bbva-counter>
        <button @click="${this.handleLogout}">Logout</button>
      </section>
    `;
  }
}
