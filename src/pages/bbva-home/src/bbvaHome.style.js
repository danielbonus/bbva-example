import { css } from 'lit-element';

export const style = css`
  :host {
    display: block;
    font-family: "Andale Mono";
  }

  .container-home {
    width: 50%;
    margin: 0 auto;
    text-align: center;
  }

  button {
    margin-top: 64px;
    color: white;
    background-color: dodgerblue;
    padding: 12px 24px;
    border: none;
    border-radius: 4px;
  }
`;
