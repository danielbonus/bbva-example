import { LitElement, html } from 'lit-element';
import '../../../packages/bbva-authentication/bbva-authentication';

export class BbvaLogin extends LitElement {
  // eslint-disable-next-line class-methods-use-this
  render() {
    return html`
      <bbva-authentication></bbva-authentication>
    `;
  }
}
