import { LitElement, html } from 'lit-element';
import { pathRoutes } from './core/router/routes';
import { connectElement } from './core/redux/reduxMixin';
import { store } from './core/redux/store';
import { actionsBbvaExample } from './core/redux/actions/actions';
import { RouterMixin, RouterProvider } from './core/router/routerMixin';
import Login from './core/login/login';

export class BbvaExampleApp extends connectElement(store)(RouterMixin(RouterProvider(LitElement))) {
  // eslint-disable-next-line class-methods-use-this
  get routes() {
    return pathRoutes;
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('on-bbva-submit', this.handleBbvaLogin);
    this.addEventListener('on-bbva-logout', this.handleBbvaLogout);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('on-bbva-submit', this.handleBbvaLogin);
    this.removeEventListener('on-bbva-logout', this.handleBbvaLogout);
  }

  async handleBbvaLogin({ detail }) {
    const { email, lastSignInTime } = await Login.registerUser(detail);
    if (email.length && lastSignInTime.length) {
      this.storeDispatch(actionsBbvaExample.profileData({ email, lastSignInTime }));
      this.navigator('/home');
    }
  }

  _cleanStorageDataProfile() {
    this.storeDispatch(actionsBbvaExample.cleanData());
  }

  async handleBbvaLogout() {
    if (await Login.singOut()) {
      this._cleanStorageDataProfile();
      this.navigator('/');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  render() {
    return html`
      <section>
        <main id="content-router">
        </main>
      </section>
    `;
  }
}
