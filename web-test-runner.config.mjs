import fs from 'fs';

const packages = fs
  .readdirSync('src/packages')
  .filter((dir) => fs.statSync(`src/packages/${dir}`).isDirectory());

export default {
  nodeResolve: true,
  coverageConfig: {
    report: true,
    reportDir: 'coverage',
    threshold: {
      statements: 90,
      branches: 60,
      functions: 70,
      lines: 90,
    },
  },
  testFramework: {
    config: {
      timeout: '6000',
    },
  },
  files: 'src/packages/*/test/*.test.js',
  groups: packages.map((pkg) => ({
    name: pkg,
    files: `src/packages/${pkg}/test/*.test.js`,
  })),
  plugins: [],
};
