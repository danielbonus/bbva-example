const copy = require('rollup-plugin-copy');
module.exports = {
  stories: ['../**/stories/*.stories.{js,md,mdx}'],
  rollup: config => {
    config.plugins.push(
      copy({
        targets: [
          { src: 'assets/**/*', dest: 'storybook-static/assets' }
        ]
      })
    )
  },
  esDevServer: {
    nodeResolve: true,
    watch: true,
    open: true,
    babel: true
  },
}
